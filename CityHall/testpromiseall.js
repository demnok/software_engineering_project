var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var app = express();
var mapOrasDate = [];

var mapJudetUrl = {};
var toateDatele = [];

//var tools=require('./tools.js');
function nr_pagini(urlJudet) {
    return new Promise(function (resolve, reject) {
        request(urlJudet, function (error, response, html) {
            var $ = cheerio.load(html);
            if (!error) {
                resolve($('.pagination').find('ul').children().last().prev().prev().text().trim());

            }
            else {
                reject(Error('Could not open county url'));
            }

        })

    })
}
function getDetails(urlJudetFinal) {

    //console.log('am intrat in getDetails');
    return new Promise(function (resolve, reject) {
        request(urlJudetFinal, function (error, response, html) {

            if (!error) {
                //          console.log('urmeaza sa apelez cheerio.load page');
                var $ = cheerio.load(html);
                //        console.log('Am apelat cheerio.load page');
                function getdatafromonepage($) {
                    var mapOrasDate = {};
                    return new Promise(function (resolve, reject) {
                        $('.articles-widget').find('li').find('h5').each(function (i) {
                            if ($(this).text().trim().includes('PRIMĂRIA')) {

                                var infoAndStuff = {};
                                var nume = $(this).text().trim();
                                nume = nume.substr(9);
                                var bla = $(this).next().text().trim();
                                if (bla.indexOf('Adresa:') !== -1) {
                                    infoAndStuff['Adresa:'] = bla.slice(bla.indexOf('Adresa:') + 8, bla.indexOf('Localitate:')).trim();
                                    //console.log(infoAndStuff['Adresa:']);

                                }
                                if (bla.indexOf('Localitate:') !== -1) {
                                    infoAndStuff['Localitate:'] = bla.slice(bla.indexOf('Localitate:') + 12, bla.indexOf('Judet:')).trim();
                                    //console.log(infoAndStuff['Localitate:']);
                                }
                                if (bla.indexOf('Judet:') !== -1) {
                                    infoAndStuff['Judet:'] = bla.slice(bla.indexOf('Judet:') + 7, bla.indexOf('Telefon:')).trim();
                                    //console.log(infoAndStuff['Judet:']);
                                }
                                if (bla.indexOf('Telefon:') !== -1) {
                                    infoAndStuff['Telefon:'] = bla.slice(bla.indexOf('Telefon:') + 9, bla.indexOf('Fax:')).trim();
                                    //console.log(infoAndStuff['Telefon:']);
                                }
                                //console.log(infoAndStuff);
                                mapOrasDate[nume] = infoAndStuff;
                                //console.log($(this).next().text().trim())
                            }
                        });
                        resolve(mapOrasDate);

                    })
                };
                //      console.log('urmeaza sa apelez getdatafrom one page');
                getdatafromonepage($).then(function (stuff) {
                    //console.log('am adaugat: ' + JSON.stringify(stuff));
                    resolve(stuff);
                }, function (error) {
                    reject(Error('Problems in function getdatafromonepage'));
                })


            }
            else {
                reject(Error('Problems in function getDetails'));
            }
        });
    });
}

function getUrlJudete() {
    return new Promise(function (resolve, reject) {
        console.log('obtin url judete');
        var urlGeneral = 'https://www.ghidulprimariilor.ro/';
        request(urlGeneral, function (error, response, html) {

                if (!error) {

                    var $ = cheerio.load(html);
                    $('.cities-widget').children().first().children().each(function (i) {
                        var data = $(this);
                        data.children().first().children().each(function (j) {
                            var dt = $(this);
                            var numeJudet = dt.text().trim().substr(0, dt.text().trim().indexOf('(')).trim();
                            var urlJudet = dt.children().first().attr('href');
                            mapJudetUrl[numeJudet] = urlJudet;

                        });


                    })
                    resolve();
                } else {
                    reject('Problems obtainging Url judete');
                }

            }
        );

    });
}


getinfo = function (urlJudet, nrp) {
    return new Promise(function (resolve, reject) {
        urlJudet = urlJudet + '/';
        var infoOras;
        //console.log('apelez cu nrp = ' + nrp);
        var rz = [];


        var arrayPromise = [];
        for (var it = 1; it <= nrp; it++) {


            var urlJudetFinal = urlJudet + it + '/';


            arrayPromise.push(getDetails(urlJudetFinal));


        }
        Promise.all(arrayPromise).then(function (resultData) {
            //  console.log('Termin promiseall');
            //console.log(resultData);
            // console.log('acum trimit mai departe result data');
            resolve(resultData);

        }, function (error) {
            reject(error('Nu s a executat array-ul de promise-uri cum trebuie '));
        });


    })


};

//return mapOrasDate;
function GetAllInfoJudet(judet) {
    console.log('obtin info despre: [' + judet + ']');
    return new Promise(function (resolve, reject) {
        var urlGeneral = 'https://www.ghidulprimariilor.ro/';
        request(urlGeneral, function (error, response, html) {

            if (!error) {

                var $ = cheerio.load(html);
                $('.cities-widget').children().first().children().each(function (i) {
                    var data = $(this);
                    data.children().first().children().each(function (j) {
                        var dt = $(this);
                        var numeJudet = dt.text().trim().substr(0, dt.text().trim().indexOf('(')).trim();
                        var urlJudet = dt.children().first().attr('href');
                        mapJudetUrl[numeJudet] = urlJudet;

                    });


                });
                var urlJudet = mapJudetUrl[judet];
                nr_pagini(urlJudet).then(function (nrp) {
                        getinfo(urlJudet, nrp).then(function (data) {

                            var m = {};
                            m[judet] = data;
                            resolve(m);
                        }, function (error) {

                            resolve(error);
                        })

                    }, function (error) {
                        console.error("Failed!", error);

                    }
                )
                //      console.log(mapJudetUrl);
            }
            else {
                console.log('Problems contacting the server');
            }
        });

    });
}


//================================================================================================================
getUrlJudete().then(function () {
    console.log('incep sa adun toate datele');

    for (var key in mapJudetUrl) {
        console.log('acum adun date despre: ' + key);
        var jd = key.substr(0);
        GetAllInfoJudet(key).then(function (data) {
            toateDatele.push(data);
            console.log('am mai terminat una');
            //console.log(data);
            //console.log('am adunat despre: ' + jd + ' urmatoarele: ');
            //console.log('cheia: '+ '['+jd+']');
        }, function (error) {
            console.log('Problems getting all the data');
            reject(error);
        });
    }

//     GetAllInfoJudet('Iasi').then(function (data) {
//                 toateDatele.push(data);
//                 console.log(data);
//                 //console.log('am adunat despre: ' + jd + ' urmatoarele: ');
//                 //console.log('cheia: '+ '['+jd+']');
//             }, function (error) {
//                 console.log('Problems getting all the data');
//             });


}, function (error) {
    console.log(error);
});
//======================================================================================================


app.get('/:judet/:oras', function (req, res) {
    // Let's scrape Anchorman 2

    var judet = req.params.judet;
    var oras = req.params.oras;

    //console.log('Judet: ' + judet);
    console.log('Judet: ' + '[' + judet + ']');


    toateDatele.forEach(function (item) {
        if (item[judet] != null) {
            res.send(JSON.stringify(item[judet]));
        }
    })

});

app.get('/:cached/:judet/:oras', function (req, res) {
    // Let's scrape Anchorman 2

    var judet = req.params.judet;
    var oras = req.params.oras;
    var cached = req.params.cached;
    console.log('oras: ' + oras);
    if (cached != 0) {
        toateDatele.forEach(function (item) {
            if (item[judet] != null) {
                console.log('Am gasit judetul');
                //res.send(JSON.stringify(item[judet]));
                console.log(item[judet]);
                item[judet].forEach(function (item2) {
                    if (item2[oras] != null) {
                        console.log('Am gasit orasul');
                        console.log(item2[oras]);
                        res.send(JSON.stringify(item2[oras]));
                    }
                })
            }
        })

    }
    else {
        getUrlJudete().then(function () {
            if (mapJudetUrl[judet] != null) {
                GetAllInfoJudet(judet).then(function (data) {
                    data[judet].forEach(function (item) {
                        console.log("New item");
                        //console.log(item);
                        if (item[oras] != null) {
                            console.log(item[oras]);
                        }
                    })
                    //console.log("Ar trebuie sa fie date despre: "+data[judet]);
                }, function (error) {
                    res.send(JSON.stringify("Problems getting the data"));
                })
            } else {
                res.send(JSON.stringify('There is no data for the specified county'));
            }


        }, function () {
            res.send(JSON.stringify('Having problems contacting the server'));
        })

    }
    //console.log('Judet: ' + judet);


    // toateDatele.forEach(function (item) {
    //     if (item[judet] != null) {
    //         res.send(JSON.stringify(item[judet]));
    //     }
    // })

});


app.get('/:cached/:judet', function (req, res) {


    var judet = req.params.judet;
    var cached = req.params.cached;
    if (cached != 0) {
        toateDatele.forEach(function (item) {
            if (item[judet] != null) {
                //TODO de avut grija de specificat ca trimite un array de map-uri nu un array sau un map chel
                res.send(JSON.stringify(item[judet]));
                console.log(item[judet]);

            } else {
                res.send(JSON.stringify('Could not find any information for what you were looking for'));
            }
        })

    }
    else {
        getUrlJudete().then(function () {
            if (mapJudetUrl[judet] != null) {
                GetAllInfoJudet(judet).then(function (data) {
                    if (data[judet] != null) {
                        JSON.stringify(data[judet]);
                    } else {
                        JSON.stringify('Could not find any information about the specified county');
                    }
                    //console.log("Ar trebuie sa fie date despre: "+data[judet]);
                }, function (error) {
                    res.send(JSON.stringify("Problems getting the data"));
                })
            } else {
                res.send(JSON.stringify('There is no data for the specified county'));
            }


        }, function () {
            res.send(JSON.stringify('Having problems contacting the server'));
        })

    }
    //console.log('Judet: ' + judet);


    // toateDatele.forEach(function (item) {
    //     if (item[judet] != null) {
    //         res.send(JSON.stringify(item[judet]));
    //     }
    // })

});
//
// app.get('/:judet', function (req, res) {
//     var judet = req.params.judet;
//     GetAllInfoJudet(judet).then(function (data) {
//         //console.log('am primit');
//         //console.log(data);
//         //console.log(data);
//         res.send(JSON.stringify(data));
//
//
//     }, function (error) {
//         reject(error);
//     });
//
// });

app.listen('8081');
console.log('Magic happens on port 8081');
exports = module.exports = app;