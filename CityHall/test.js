var chai = require('chai');
var mocha = require('mocha');

var server = require('./server.js');
var should = chai.should();
var chaiHttp = require('chai-http');
var describe = mocha.describe;
var expect = require('chai').expect;
chai.use(chaiHttp);

describe('Regression testing for routing.', function(){
    this.timeout(99999999);
    it('should list all the information about each city hall in Iasi', function(done) {
        chai.request(server).get('/0/Iasi/').end(function(err, res) {
            res.should.have.status(200);
            done();
        }, function(err) {
            return err;
        });
    })
    it('should list all the information about BIVOLARI', function(done) {
        chai.request(server).get('/0/Iasi/BIVOLARI').end(function(err, res) {
            res.should.have.status(200);
            done();
        }, function(err) {
            return err;
        })
    })

    it('should fail to find the cit hall ORASFALS', function(done) {
        chai.request(server).get('/0/Iasi/ORASFALS').end(function(err, res) {
            res.should.have.status(404);
            done();
        }, function(err) {
            return err;
        })
    })
    it('should fail to find the county JUDTFALS', function(done) {
        chai.request(server).get('/0/JUDETFALS').end(function(err, res) {
            res.should.have.status(404);
            done();
        }, function(err) {
            return err;
        })
    })
});
