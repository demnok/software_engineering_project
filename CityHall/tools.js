var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var app = express();
var mapOrasDate = [];
//var tools=require('./tools.js');
function nr_pagini(urlJudet) {
    return new Promise(function (resolve, reject) {
        request(urlJudet, function (error, response, html) {
            var $ = cheerio.load(html);
            if (!error) {
                resolve($('.pagination').find('ul').children().last().prev().prev().text().trim());

            }
            else {
                reject(Error('Could not open county url'));
            }

        })

    })
}
function getDetails(urlJudetFinal) {

    //console.log('am intrat in getDetails');
    return new Promise(function (resolve, reject) {
        request(urlJudetFinal, function (error, response, html) {

            if (!error) {
                //          console.log('urmeaza sa apelez cheerio.load page');
                var $ = cheerio.load(html);
                //        console.log('Am apelat cheerio.load page');
                function getdatafromonepage($) {
                    var mapOrasDate = {};
                    return new Promise(function (resolve, reject) {
                        $('.articles-widget').find('li').find('h5').each(function (i) {
                            if ($(this).text().trim().includes('PRIMĂRIA')) {

                                var InfoAndStuff={};
                                var nume = $(this).text().trim();
                                nume = nume.substr(9);
                                var bla=$(this).next().text().trim();
                                if(bla.indexOf('Adresa:') !== -1){
                                    InfoAndStuff['Adresa:']=bla.slice(bla.indexOf('Adresa:')+8,bla.indexOf('Localitate:'));
                                }
                                mapOrasDate[nume] = $(this).next().text().trim();
                                //console.log($(this).next().text().trim())
                            }
                        });
                        resolve(mapOrasDate);

                    })
                };
                //      console.log('urmeaza sa apelez getdatafrom one page');
                getdatafromonepage($).then(function (stuff) {
                    //console.log('am adaugat: ' + JSON.stringify(stuff));
                    resolve(stuff);
                }, function (error) {
                    reject(Error('Problems in function getdatafromonepage'));
                })


            }
            else {
                reject(Error('Problems in function getDetails'));
            }
        });
    });
}


getinfo = function (urlJudet, nrp) {
    return new Promise(function (resolve, reject) {
        urlJudet = urlJudet + '/';
        var infoOras;
        //console.log('apelez cu nrp = ' + nrp);
        var rz = [];


        var arrayPromise = [];
        for (var it = 1; it <= nrp; it++) {


            var urlJudetFinal = urlJudet + it + '/';


            arrayPromise.push(getDetails(urlJudetFinal));


        }
        Promise.all(arrayPromise).then(function (resultData) {
            //  console.log('Termin promiseall');
            //console.log(resultData);
            // console.log('acum trimit mai departe result data');
            resolve(resultData);

        }, function (error) {
            reject(error('Nu s a executat array-ul de promise-uri cum trebuie '));
        });


    })


};

//return mapOrasDate;
function GetAllInfoJudet(judet) {
    return new Promise(function (resolve, reject) {
        var urlGeneral = 'https://www.ghidulprimariilor.ro/';
        request(urlGeneral, function (error, response, html) {

            if (!error) {
                var mapJudetUrl = {};
                var $ = cheerio.load(html);
                $('.cities-widget').children().first().children().each(function (i) {
                    var data = $(this);
                    data.children().first().children().each(function (j) {
                        var dt = $(this);
                        var numeJudet = dt.text().trim().substr(0, dt.text().trim().indexOf('(')).trim();
                        var urlJudet = dt.children().first().attr('href');
                        mapJudetUrl[numeJudet] = urlJudet;

                    });


                });
                var urlJudet = mapJudetUrl[judet];
                nr_pagini(urlJudet).then(function (nrp) {
                        //                      console.log('am primit ca raspuns : ' + nrp);
                        getinfo(urlJudet, nrp).then(function (data) {
                            //                        console.log(data);
                            //                      console.log('primesc raspunsul');
                            resolve(data);
                        }, function (error) {

                            resolve(error);
                            //                    console.log('Nu sa executat getInfo cum trebuie' + error);
                        })

                    }, function (error) {
                        console.error("Failed!", error);

                    }
                )
                //      console.log(mapJudetUrl);
            }
        });

    });
}


app.get('/:judet/:oras', function (req, res) {
    // Let's scrape Anchorman 2

    var judet = req.params.judet;
    var oras = req.params.oras;

    //console.log('Judet: ' + judet);
    console.log('Oras: ' + oras);


    GetAllInfoJudet(judet).then(function (data) {
        //console.log('am primit');
        //console.log(data);
        console.log(data);
        data.forEach(function(item){
            if(item[oras]!=null){
                res.send(JSON.stringify(item[oras]));
                console.log('oras 2 : '+oras);
            }
        });


    }, function (error) {
        reject(error);
    });
});


app.get('/:judet', function (req, res) {
    var judet = req.params.judet;
    GetAllInfoJudet(judet).then(function (data) {
        //console.log('am primit');
        //console.log(data);
        console.log(data);
        res.send(JSON.stringify(data));


    }, function (error) {
        reject(error);
    });

});

app.listen('8081');
console.log('Magic happens on port 8081');
exports = module.exports = app;