var express = require('express')
var request = require('request')
var cheerio = require('cheerio')
var http 		= require('http')
var cache 	= require('apicache').middleware

var univService = require('./university.js')


var app = express()

var server = http.createServer(app);
module.exports = server;
server.listen(3000, function () {
	console.log('Education app listening on port 3000!')
})

app.use(cache('1440 minutes'));

app.get('/', function (req, res, next) {
	//console.log("Waiting for response...");
	univService.getFunctionalies().then(function(result){
		//console.log("got response ");
		res.send(JSON.stringify(result));
	}, function(error) {
			//console.log("got error "+ error);
			res.status(500).send(JSON.stringify(error));
	});
})

app.get('/topUniversities/:country', function(req, res, next){
	var country = req.params.country;
	//console.log("Request for: " + country);
	univService.getTopUniversities(country).then(function(result){
		//console.log("got res " + JSON.stringify(result));
		res.send(JSON.stringify(result));
	}, function(error) {
			//console.log("got error "+ JSON.stringify(error));
			res.status(500).send(JSON.stringify(error));
	});	
})

app.get('/universities', function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');
	res.send(univService.getUniversities());
})

app.get('/universities/:univ', function	(req, res, next) {
	var univ = req.params.univ;
	//console.log('Reguest for ' + univ);
	univService.getSpecificUniversity(univ).then(function(result) {
		res.send(JSON.stringify(result));
	}, function(error) {
		res.status(404).send(JSON.stringify(error));
	})
})

app.get('/universities/:univ/faculties', function(req, res, next){
	var univ = req.params.univ;
	//console.log("Request for listing " + univ + " faculties");
	univService.getAllFaculties(univ).then(function(result) {
		res.send(JSON.stringify(result));
	}, function(error) {
		res.status(404).send(JSON.stringify(error));
	})
})

app.get('/universities/:univ/faculties/:faculty/address', function(req, res, next){
	var univ = req.params.univ;
	var faculty = req.params.faculty;
	//console.log("Request for listing " + univ +" " + faculty + " address");
	univService.getFacultyAddress(univ, faculty).then(function(result) {
		res.send(JSON.stringify(result));
	}, function(error) {
		res.status(404).send(JSON.stringify(error));
	})
})

app.get('/universities/:univ/faculties/:faculty/website', function(req, res, next){
	var univ = req.params.univ;
	var faculty = req.params.faculty;
	//console.log("Request for listing " + univ +" " + faculty + " website");
	univService.getFacultyWebsite(univ, faculty).then(function(result) {
		res.send(JSON.stringify(result));
	}, function(error) {
		res.status(404).send(JSON.stringify(error));
	})
})

app.get('/universities/:univ/timetable', function(req, res, next) {
	var univ = req.params.univ;
	//console.log("Request for listing " + univ + " timetable");
	univService.getTimetable(univ).then(function(result) {
		res.send(JSON.stringify(result));
	}, function(error) {
		res.status(404).send(JSON.stringify(error));
	})
})

app.get('/universities/uaic/facultatea-de-informatica/timetable', function(req, res, next){
	//console.log("Request for fac informatica timetable");
	var resource = "https://www.info.uaic.ro/bin/Structure/Administration?language=en";
	request(resource, function(error, response, body) {
		if(error){
			//console.log(error);
			res.status(404).send({
				statusCode: error.code,
				error: "Resource not found"
			});
		}else {
			var $ = cheerio.load(body);
			var secretary = $('div[class=layoutsubsection]').next().text().split("Timetable:")[1].split("Aux")[0].trim();
			//console.log("Secretary: " + JSON.stringify({'secretary':secretary}));
			res.send(JSON.stringify({'secretary':secretary}));
		}
	})
})


app.get('/*',function(req,res)
{
    res.status(400).send({statusCode: 400, error: "Bad request"});
 });
