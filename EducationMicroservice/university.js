var express = require('express')
var request = require('request')
var cheerio = require('cheerio')
var Q 			= require('q')


var getCountries = function(resource, deliver, reportError) {
	var countries = [];
	request(resource, function(error, response, body) {
		if(error){
			//console.log(error);
			reportError(error);
		}else {
			var $ = cheerio.load(body);
			$('ul[class="dropdown-menu drop-main-search dropdown-searchable dropdown-remove-all-programs-cookie"]').children().each(function(index, el) {
				var link = $(this).children().first().attr('href');
				var country = link.split('https://www.masterstudies.com/universities')[1].split('/')[1];
				var funct = {};
				funct[country] = '/topUniversities/' + country;
				countries.push(funct);
			})
			deliver(countries);
		}
	})
	
}

module.exports = {

getFunctionalies : function () {
	var deferred = Q.defer();
	var resource = "https://www.masterstudies.com/universities/";
	getCountries(resource, function deliverResults(result){
		var functionalities = {
			"topUniversities" : result,
			"universitiesList" : "/universities",
			"facultiesList" : {
				"uaic" 	: "/universities/uaic/faculties",
				"usv" 	: "/universities/usv/faculties"
			},
			"timetable"				: {
				"uaic"											: "/universities/uaic/timetable",
				"facultatea-de-informatica" : "/universities/uaic/facultatea-de-informatica/timetable"
			},
			"address" : {
				"uaic" : "/universities/uaic/faculties/:faculty/address"
			},
			"website" : {
				"uaic" : "/universities/uaic/faculties/:faculty/website"
			}
		}
		var resp = {'functionalities': functionalities};	
		deferred.resolve(resp);
	}, function(error) {
			deferred.reject({
				statusCode: error.code,
				error: "Resource not found"
			});
	});
	return deferred.promise;
},
getTopUniversities : function(country) {
	var deferred = Q.defer();
	var resource = "https://www.masterstudies.com/universities/" + country + "/";
	request(resource, function(error, response, body) {
		if(error){
			deferred.reject({
				statusCode: error.code,
				error: "Resource not found"
			});
		}else {
			var $ = cheerio.load(body);
			var universities = [];
			$('a','h4[class=listing-title]').each(function(index) {
				universities.push($(this).text().replace(/[\t\n]+/g, "").trim());
			})
			deferred.resolve(universities);
		}
	})
	return deferred.promise;
},
getUniversities : function() {
	var resp = [
						{
							uaic: '"Alexandru Ioan Cuza" University of Iasi'
						},
						{
							usv: '"Stefan cel Mare" University of Suceava'
						}
	];
	return resp;
},
getSpecificUniversity : function(univ) {
	var deferred = Q.defer();

	if(univ == "uaic") {
		var resp = {uaic: '"Alexandru Ioan Cuza" University of Iasi'};
		deferred.resolve(resp);
	} else if(univ == "usv") {
		var resp = {usv: '"Stefan cel Mare" University of Suceava'};
		deferred.resolve(resp);
	} else {
		deferred.reject({
			statusCode: 404,
			error: "University not found"
		});
	}
	return deferred.promise;
},
getAllFaculties : function(univ) {
	var deferred = Q.defer();
	if(univ == "uaic"){
		var resource = "http://www.uaic.ro/studii/facultati-2/";
		request(resource, function(error, response, body) {
			if(error){
				//console.log(error);
				deferred.reject({
					statusCode: error.code,
					error: "Resource not found"
				});
			}else {
				var $ = cheerio.load(body);
				//console.log("Loaded " + $('title').text());
				var faculties = [];
				$('div[class=fusion-column-wrapper]').each(function(index) {
					faculties.push($(this).text().replace(/[\t\n]+/g,""));
				})
				//console.log("Faculties:" + faculties);
				deferred.resolve(faculties);
			}
		}) 
	}else if(univ == "usv"){
		var resource = "http://www.usv.ro/index.php/en";
		request(resource, function(error, response, body) {
			if(error){
				//console.log(error);
				deferred.reject({
					statusCode: error.code,
					error: "Resource not found"
				});
			}else {
				var $ = cheerio.load(body);
				var faculties = [];
				$('li','ul[class=kwicks]').each(function(index) {
					faculties.push($(this).text());
				})
				//console.log("Faculties: " + JSON.stringify(faculties));
				deferred.resolve(faculties);
			}		
		})
	} else {
		deferred.reject({
			statusCode: 404,
			error: "University not found"
		});
	}
	return deferred.promise;
},
getFacultyAddress : function(univ, faculty) {
	var deferred = Q.defer();
	if(univ == "uaic"){
		var resource = "http://www.uaic.ro/studii/facultati-2/" + faculty + "/";
		request(resource, function(error, response, body) {
			if(error){
				//console.log(error);
				deferred.reject({
					statusCode: error.code,
					error: "Resource not found"
				});
			}else {
				var $ = cheerio.load(body);
				//console.log("Loaded " + $('h1[class=entry-title]').text());
				if($('h1[class=entry-title]').text().match(/error/i)){
					//console.log("not faculty of the university");
					deferred.reject({
						statusCode: 400,
						error: 'Bad request - inexistent faculty'
					})
				}
				var address = $('p','div[class=post-content]').text().split("Telefon")[0].split(/[\t\n]+/)[0];
				/*
				* remove word "adresa/adresă" from result
				*/
				var adres = "adres";
				if(address.search(/adres/i) != -1){
					address = address.split(":")[1];
				}
				//console.log("Address:" + address);
				deferred.resolve({'address': address});
			}
		})
	} else {
		deferred.reject({
			statusCode: 404,
			error: "University not found"
		});
	}
	return deferred.promise;
},
getFacultyWebsite : function(univ, faculty) {
	var deferred = Q.defer();
	if(univ == "uaic"){
		var resource = "http://www.uaic.ro/studii/facultati-2/" + faculty + "/";
		request(resource, function(error, response, body) {
			if(error){
				//console.log(error);
				deferred.reject({
					statusCode: error.code,
					error: "Resource not found"
				});
			}else {
				var $ = cheerio.load(body);
				//console.log("Loaded " + $('title').text());
				if($('h1[class=entry-title]').text().match(/error/i)){
					//console.log("not faculty of the university");
					deferred.reject({
						statusCode: 400,
						error: 'Bad request - inexistent faculty'
					});
				}else {					
					var website = $('p','div[class=post-content]').text().split("Website: ")[1].split("Decan")[0];
					//console.log("Website:" + website);
					deferred.resolve({'website': website});
				}
			}
		})
	}else {
		deferred.reject({
			statusCode: 404,
			error: "University not found"
		});
	}
	return deferred.promise;
}, 
getUnivTimetable : function(univ){
	var deferred = Q.defer();	
	if(univ == "uaic"){
		var resource = 'http://www.uaic.ro/despre-uaic/contact/';
		request(resource, function(error, response, body) {
			if(error){
				//console.log(error);
				deferred.reject({
					statusCode: error.code,
					error: "Resource not found"
				});
			}else {
				var $ = cheerio.load(body);
				var timetable = $('div[class=fusion-column-wrapper]')
							.children().slice(5, 6).text();
				//console.log("timetable: " + JSON.stringify({'timetable':timetable}));
				deferred.resolve({'timetable':timetable});
			}
		})
	}else {
		deferred.reject({
			statusCode: 404,
			error: "Unknown university"
		});		
	}
	return deferred.promise;
}
};