var assert 			= require('assert');
var rewire 			= require('rewire');
var univService = rewire('../university.js')

describe('univService', function() {
	describe('#getFunctionalies()', function() {
		it('should return an object of functionalities', function() {
			univService.getFunctionalies().then(function(result) {
				assert(result.functionalities != undefined);
			})
		})
	});

	describe('#getCountries(resourceLink)', function() {
		it('should return error - resource not found', function() {
			var resourceLink = "http://mocked.wrong.link";
			var getCountries = univService.__get__('getCountries');
			var countriesResp = getCountries(resourceLink, function(resp) {}, function(err) {
				assert(err.error == "Resource not found");
			});
		})
	});
	describe('#getTopUniversities(country)', function() {

	})
})