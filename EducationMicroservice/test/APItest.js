var 	assert 		= require('assert')
const chai 			= require('chai');
const server		= require('../app.js');
const should 		= chai.should();
const chaiHttp 	= require('chai-http');

chai.use(chaiHttp);
	
describe('APITests', function() {
 this.timeout(99999999);
	describe('/GET /', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return an object containing the functionalities', function(done) {
			chai.request(server)
					.get('/')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result.functionalities != undefined);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /topUniversities/:country', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return an object containing the list of the universities', function(done) {
			chai.request(server)
					.get('/topUniversities/Romania')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result != undefined);
						assert(result instanceof Array)
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /topUniversities/:country', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return empty list for a not existing country', function(done) {
			chai.request(server)
					.get('/topUniversities/Romujhania')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result instanceof Array);
						assert(result.length == 0);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return a list of JS Ojects', function(done) {
			chai.request(server)
					.get('/universities')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result instanceof Array);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return an object with the requested university', function(done) {
			chai.request(server)
					.get('/universities/uaic')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result.uaic != undefined);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return 404 University not found', function(done) {
			chai.request(server)
					.get('/universities/uaeefwic')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(404);
						assert(result.error == "University not found");
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ/faculties', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return a list of faculties', function(done) {
			chai.request(server)
					.get('/universities/usv/faculties')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result instanceof Array);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ/faculties', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return 404 University not found', function(done) {
			chai.request(server)
					.get('/universities/usfesv/faculties')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(404);
						assert(result.error == "University not found");
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ/faculties/:faculty/address', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return an address', function(done) {
			chai.request(server)
					.get('/universities/uaic/faculties/facultatea-de-informatica/address')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(200);
						assert(result.address != undefined);
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ/faculties/:faculty/address', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return 404 University not found', function(done) {
			chai.request(server)
					.get('/universities/usfesv/faculties/fac/address')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(404);
						assert(result.error == "University not found");
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /universities/:univ/faculties/:faculty/address', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return 404 Bad request - inexistent faculty', function(done) {
			chai.request(server)
					.get('/universities/uaic/faculties/fac/address')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(404);
						assert(result.error == "Bad request - inexistent faculty");
						done();
					}, function(err) {
							return err;
					});
		})
	});

	describe('/GET /RandomRoute', function() {
/*		before(function() {
			this.skip();
		});*/
		it('should return status 400 bad request', function(done) {
			chai.request(server)
					.get('/RandomRoute')
					.end(function(err, response){
						var result = JSON.parse(response.text);
						response.should.have.status(400);
						assert(result.statusCode == 400);
						assert(result.error == "Bad request");
						done();
					}, function(err) {
							return err;
					});
		})
	});

})