var chai = require('chai');
var server = require('../app.js');
var should = chai.should();
var chaiHttp = require('chai-http');

chai.use(chaiHttp);

describe('Regression testing for routing.', function(){
  this.timeout(99999999);
    it('should list the contact info', function(done) {
      chai.request(server).get('/contactInfo').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      });
    })
    it('should list the 2nd stolen cars page', function(done) {
      chai.request(server).get('/stolenCars/2').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      })
    })
    it('should list the terrorism alert level', function(done) {
      chai.request(server).get('/terrorismLevel').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      })
    })
    it('should list the emergency phone number', function(done) {
      chai.request(server).get('/emergencyPhone').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      })
    })
    it('should list the list of tips', function(done) {
      chai.request(server).get('/tipps').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      })
    })
    it('should list the 3rd page of info traffic', function(done) {
      chai.request(server).get('/infoTraffic/3').end(function(err, res) {
        res.should.have.status(200);
        done();
      }, function(err) {
        return err;
      })
    })
    it('should return 404 for unknown route', function(done) {
      chai.request(server).get('/WhyUDoDis_Senpai').end(function(err, res) {
        res.should.have.status(404);
        done();
      }, function(err) {
        return err;
      })
    })
  });
