var express = require('express')
var app = express()
var cors = require('cors')
var bodyParser = require('body-parser');
var http = require('http');
var timeout = require('connect-timeout')
var apicache = require('apicache');
var q = require('q');

var cache = apicache.middleware
const request = require('request');
const cheerio = require('cheerio');
const sleep = require('sleep');

//var policeManager = require('./police_microservice.js');

app.use(bodyParser.json());
app.use(cors());
app.use(timeout(120000));

app.use(cache('1440 minutes'));

app.get('/', function(req, res) {
  res.redirect('https://bitbucket.org/demnok/software_engineering_project/wiki/Police_Microservice%20Documentation');
})

app.get('/contactInfo', function (req, res) {
  var pageToVisit = "https://is.politiaromana.ro/ro/i-p-j-iasi/politii-municipale-si-orasanesti/pol-mun-is";
  //console.log("Visiting page " + pageToVisit);
  request(pageToVisit, function(error, response, body) {
    if(error) {
      console.log("Error: " + error);
    }
    //console.log("Status code: " + response.statusCode);

    if(response.statusCode === 200) {
      var $ = cheerio.load(body);
      //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
    }
    else {
      console.log("The page was not found.");
      return "The page was not found.";
    }

    var data = {};
    $('div.stireDesc').each(function( index ) {
      var contactInfo = $(this).text();
      data = data + contactInfo;
    });

    data = data.split("\r\n");
    var actualData = [];
    data.forEach(function(entry) {
      if(!entry.isEmpty()) {
        actualData.push(entry.trim());
      }
    })
    var object = {};
    var temp = {};
    actualData.forEach(function (value, index) {
      if(index % 9 == 1) {
        object[value] = [];
        temp = value;
      }
      else {
        var quot = index / 9;
        var realDeal = value.split(/:/).slice(0).join(':');
        object[temp] += realDeal;
      }

    })
    JSON.stringify(object);
    res.send(object);
  })
})

app.get('/stolenCars/:pageN', function (req, res) {
  var pageNumber = parseInt(req.params.pageN, 10);
  if(typeof(pageNumber) == "number") {
  var initialString = "https://www.politiaromana.ro/ro/autovehicule-furate&page=";
  var cont = pageNumber;
  var data = {};
  var actualData = [];
  //while (cont != 2000) {
    var pageToVisit = initialString + cont;
    //console.log(pageToVisit);
    request(pageToVisit, function(error, response, body) {
      if(error) {
        console.log("Error: " + error);
      }
      //console.log("Status code: " + response.statusCode);

      if(response.statusCode === 200) {
        var $ = cheerio.load(body);
        //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
      }
      else {
        console.log("The page was not found.");
        res.send("The page was not found.");
      }

      $('div.listBoxAfisare').each(function( index ) {
        var contactInfo = $(this).text().trim();
        data = data + contactInfo;
      });
      data = data.split("\r\n");

      var actualData = [];
      data.forEach(function(entry) {
        if(!entry.isEmpty()) {
          actualData.push(entry.trim());
        }
      })
      var object = {};
      var temp = {};
      actualData.forEach(function (value, index) {
        if(index % 3 == 1) {
          object[value] = [];
          temp = value;
        }
        else {
          var quot = index / 3;
          var realDeal = value;
          object[temp] += realDeal;
        }

      })
      JSON.stringify(object);
      //console.log(object);
      res.send(object);
      sleep.sleep(1);
    })
    cont++;
  //}
  }
  else {
    console.log("Bad Parameter.")
    res.send("Bad Parameter.")
    //return "Bad Parameter.";
  }
})

app.get('/terrorismLevel', function (req, res) {
  var pageToVisit = "https://www.politiaromana.ro/ro/sistemul-national-de-alerta-terorista";
  //console.log("Visiting page " + pageToVisit);
  request(pageToVisit, function(error, response, body) {
    if(error) {
      console.log("Error: " + error);
    }
    //console.log("Status code: " + response.statusCode);

    if(response.statusCode === 200) {
      var $ = cheerio.load(body);
      //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
    }
    else {
      console.log("The page was not found.");
      return "The page was not found.";
    }

    var data = {};
    $('span.nivelCurent_snatB').each(function( index ) {
      var contactInfo = $(this).text();
      data = data + contactInfo;
    });

    data = data.split("\r\n");
    var actualData = [];
    data.forEach(function(entry) {
      if(!entry.isEmpty()) {
        actualData.push(entry.trim());
      }
    })
    var final = String(actualData).split(/]/).slice(1);
    //console.log(final);
    //return final;
    res.send(final);
  })
})

app.get('/tipps', function (req, res) {
  var pageToVisit = "https://www.politiaromana.ro/ro/utile/sfaturi-utile";
  var data = {};
  var actualData = [];
  var allAbsoluteLinks = [];
  var finalData = [];
  var object = {};
  var finalObject = {};
  var temp = {};
  //console.log("Visiting page " + pageToVisit);

  request(pageToVisit, function(error, response, body) {

    if(error) {
      console.log("Error: " + error);
    }
    //console.log("Status code: " + response.statusCode);

    if(response.statusCode === 200) {
      var $ = cheerio.load(body);
      //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
    }
    else {
      console.log("The page was not found.");
      return "The page was not found.";
    }

    var absoluteLinks = $("a[href^='https://www.politiaromana.ro/ro/utile/sfaturi-utile/']");
    data = {};
    absoluteLinks.each(function() {
        allAbsoluteLinks.push($(this).attr('href'));
    });

    var promiseArray = [];
    allAbsoluteLinks = uniq(allAbsoluteLinks);
    allAbsoluteLinks.forEach(function (entry){
      var pageToVisit = entry;
      var promise = q.Promise(function(resolve, reject) {
        request(pageToVisit, function(error, response, body) {
          if(error) {
            reject(error);
          }
          //console.log("Status code: " + response.statusCode);

          if(response.statusCode === 200) {
            var $ = cheerio.load(body);
            //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
          }
          else {
            console.log("The page was not found.");
            reject("The page was not found.")
          }

          $('div.contentList').each(function( index ) {
            var contactInfo = $(this).text();
            data = data + contactInfo;
          });

          data = data.split("\r\n");

          var dataStream = String(data).trim();
          dataStream = String(dataStream).replace(/^[^a-z\d]*|[^a-z\d]*$/gi, '');
          dataStream = String(dataStream).replace(/^[^a-z\d]*|[^a-z\d]*$/gi, '');
          dataStream = String(dataStream).replace(/,/gi, ' ');
          dataStream = String(dataStream).split(/]/).slice(1);
          dataStream = String(dataStream).trim();
          //dataStream = String(dataStream).replace(/ /gi, '.');
          object[$('title').text()] = dataStream;
          //console.log(dataStream);
          //console.log(object);
          resolve(object);
          //console.log(actualData);
        })


      })
      promiseArray.push(promise);
      //object[$('title').text()] = dataStream;
        //console.log(object);
    });

    q.all(promiseArray).then(function(data){
      //console.log(data[data.length - 1]);
      res.send(data[data.length - 1]);
    })
    JSON.stringify(object);
  })
})

app.get('/emergencyPhone', function (req, res) {
  var pageToVisit = "https://www.politiaromana.ro/ro/sistemul-national-de-alerta-terorista";
  //console.log("Visiting page " + pageToVisit);
  request(pageToVisit, function(error, response, body) {
    if(error) {
      console.log("Error: " + error);
    }
    //console.log("Status code: " + response.statusCode);

    if(response.statusCode === 200) {
      var $ = cheerio.load(body);
      //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
    }
    else {
      console.log("The page was not found.");
      return "The page was not found.";
    }

    var data = {};
    $('div.headerRight').each(function( index ) {
      var contactInfo = $(this).text();
      data = data + contactInfo;
    });

    data = data.split("\r\n");
    var actualData = [];
    data.forEach(function(entry) {
      if(!entry.isEmpty()) {
        actualData.push(entry.trim());
      }
    })

    var final = String(actualData).split(/]/).slice(1);
    var nr = String(final).substring(1,4);
    final = String(final).substring(4);
    final += ' ' + nr + '.';
    //console.log(final);
    //return final;
    res.send(final);
  })
})

app.get('/infoTraffic/:pageN', function (req, res) {
  var pageNumber = parseInt(req.params.pageN, 10);
  if(typeof(pageNumber) == "number") {
  var initialString = "https://www.politiaromana.ro/ro/info-trafic&page=";
  var cont = 1;
  var data = {};
  var actualData = [];
  //while (cont != 2000) {
    var pageToVisit = initialString + cont;
    //console.log(pageToVisit);
    request(pageToVisit, function(error, response, body) {
      if(error) {
        console.log("Error: " + error);
      }
      //console.log("Status code: " + response.statusCode);

      if(response.statusCode === 200) {
        var $ = cheerio.load(body);
        //console.log("Page title:  " + $('title').text() + " Loaded successfully.");
      }
      else {
        console.log("The page was not found.");
        return "The page was not found.";
      }

      $('h3').each(function( index ) {
        var contactInfo = $(this).text().trim();
        data = data + contactInfo;
      });
      data = data.split("\r\n");

      var actualData = [];
      data.forEach(function(entry) {
        if(!entry.isEmpty()) {
          actualData.push(entry.trim());
        }
      })
      JSON.stringify(actualData);
      //console.log(actualData);
      res.send(actualData);
      //sleep.sleep(1);
    })
    cont++;
  //}
  }
  else {
    console.log("Bad Parameter.")
    res.send("Bad Parameter.");
    //return "Bad Parameter.";
  }
})

String.prototype.isEmpty = function() {
    return (this.length === 0 || !this.trim());
};

function uniq(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}
var server = http.createServer(app);
server.listen(3005, function () {
  console.log('Example app listening on port 3005!')
})

module.exports = server;
