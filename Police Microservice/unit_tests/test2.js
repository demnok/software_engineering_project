var chai = require('chai');
var police = require('../police_microservice.js');
var should = chai.should();
var expect = chai.expect;

describe('Checking objects integrity', function() {
  this.timeout(10000);
  it('should have the following keys: Poliţia Municipiului Iaşi, Secţia 1 Poliţie Iaşi, Secţia 2 Poliţie Iaşi', function(done) {
    police.getContactInfo().then(function(data) {
      //console.log(data);
      var exp = ['Poliţia Municipiului Iaşi', 'Secţia 1 Poliţie Iaşi', 'Secţia 2 Poliţie Iaşi'];
      data.should.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should not have the following keys: wrong key', function(done) {
    police.getContactInfo().then(function(data) {
      //console.log(data);
      var exp = ['wrong key'];
      data.should.not.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should have the following key: Număr înmatriculareDJ05NLV', function(done) {
    police.getStolenCarsList(2).then(function(data) {
      //console.log(data);
      var exp = ['Număr înmatriculareDJ05NLV'];
      data.should.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should not have the following key: gg_wp', function(done) {
    police.getStolenCarsList(2).then(function(data) {
      //console.log(data);
      var exp = ['gg_wp'];
      data.should.not.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should be [ NIVEL PRECAUT ]', function(done) {
    police.getTerroristAlertLevel().then(function(data) {
      var exp = [ 'NIVEL PRECAUT' ];
      JSON.stringify(data).should.equal(JSON.stringify(exp));
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should not be gg_wp', function(done) {
    police.getTerroristAlertLevel().then(function(data) {
      var exp = [ 'gg_wp' ];
      JSON.stringify(data).should.not.equal(JSON.stringify(exp));
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should have the following key: Poliția Română - Unde ne adresăm?', function(done) {
    police.getUsefullTipps().then(function(data) {
      //console.log(data);
      var exp = ['Poliția Română - Unde ne adresăm?'];
      data.should.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should NOT have the following key: GG_WP?', function(done) {
    police.getUsefullTipps().then(function(data) {
      //console.log(data);
      var exp = ['GG_WP'];
      data.should.not.include.keys(exp);
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should be Pentru situaţii de urgenţă, apelaţi 112.', function(done) {
    police.getEmergencyTelephoneInfo().then(function(data) {
      var exp = 'Pentru situaţii de urgenţă, apelaţi 112.';
      JSON.stringify(data).should.equal(JSON.stringify(exp));
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
  it('should not be gg_wp', function(done) {
    police.getEmergencyTelephoneInfo().then(function(data) {
      var exp = 'gg_wp';
      JSON.stringify(data).should.not.equal(JSON.stringify(exp));
      done();
    }, function(err) {
      console.log(JSON.stringify(err));
      done();
    }).catch(done);
  });
})
