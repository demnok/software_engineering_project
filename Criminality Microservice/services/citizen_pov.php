<?php

function get_categories($data) {
    preg_match_all('!<tr><td class="columnWithName">\s*(.*?)\s*\<\/td>!',$data,$match);
    return $match[1];
}

function get_coefficient($data) {
    preg_match_all('!<tr><td class="columnWithName">.*?<div id.*?>\s*(\d{1,2}\.\d{2})!',$data,$match);
    return $match[1];
}

function get_overall($data) {
    preg_match_all('!<tr><td class="columnWithName">.*?<span.*?>\s*(.*?)\s*<\/span>!',$data,$match);
    return $match[1];
}

function get_reportees($data) {
    preg_match_all('!<p><span class="reportees">\s*(.*?)\s*<\/span>!',$data,$match);
    return $match[1];
}

function scrape_numbeo($input, $is_city) {

    $url = '';

    if ($is_city == 1) {
        $url = "https://www.numbeo.com/crime/in/" . $input;
    } else {
        $url = "https://www.numbeo.com/crime/country_result.jsp?country=" . $input;
    }

    $result = get_data($url);
    
    $crime_data = array();


    preg_match_all('!Cannot find city id!', $result, $match);

    if ($match[0]) {
        return 'No data found or data not relevant.';
    }
             
    $crime_data['categories'] = get_categories($result);
    
    $crime_data['coefficient'] = get_coefficient($result);

    $crime_data['overall'] = get_overall($result);
    
    $crime_data['reportees'] = get_reportees($result);

    
    return $crime_data;
    
}

function general_info($location) {

    $countries = ['Romania', 'Moldova'];
    $is_city = 1;

    if (in_array($location, $countries)) {
        $is_city = 0;
    }

    return scrape_numbeo($location, $is_city);

}

?>