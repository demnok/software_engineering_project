<?php

function checkCache($requestService, $requestServiceFunction, $data) {
    $results = array();
    foreach ($data as $dataPiece)
    {
        $dataPiece = ucfirst($dataPiece);
        $path = __ROOT__."\\cache\\".$requestService."\\".$requestServiceFunction."\\".$dataPiece;

        if (isCached($path)) {
            $results[$dataPiece] = getCache($path);
        }
        else {
            $results[$dataPiece] = $requestServiceFunction($dataPiece);
            cacheSave($path, $results[$dataPiece]);
        }
    }

    return json_encode($results, JSON_PRETTY_PRINT);
}

function isCached($path) {

    if (!file_exists($path) || filemtime($path) < time() - 21600) {
        return false;
    }
    return true;

}

function getCache($path) {
    return unserialize(file_get_contents($path));
}

function cacheSave($path, $new_data) {
    file_put_contents($path, serialize($new_data));
}


?>