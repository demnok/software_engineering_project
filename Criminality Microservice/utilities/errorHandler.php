<?php

function returnError($string, $valid_input=[]) {

    $return_data = ['error' => $string];

    if ($valid_input) {
        $return_data += array('valid_input' => $valid_input);
    }

    echo json_encode($return_data, JSON_PRETTY_PRINT);

    exit();
}

?>