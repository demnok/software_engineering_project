<?php

require_once('../services/citizen_pov.php');
require_once('../utilities/dataRetriever.php');

function print_($data) {
    print_r($data);
    echo PHP_EOL;
}

class get_categories_tests {

    public static function should_return_true_when_one_element_only() {
        $result = get_categories('<tr><td class="columnWithName">1</td></tr>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_more_than_one_element() {
        $result = get_categories('<div><tr><td class="columnWithName">1</td></tr>
                                       <tr><td class="columnWithName">1</td></tr></div>');

        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i] != '1') {
                return 'failed';
            }
        }
        return 'passed';
    }

    public static function should_return_true_when_key_data_is_padded_with_spaces() {
        $result = get_categories('<tr><td class="columnWithName">    1    </td></tr>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_false_when_key_data_is_not_enclosed_in_a_tr() {
        $result = get_categories('<td class="columnWithName">    1    </td>');

        if (count($result) != 0) {
            return 'failed';
        }
        return 'passed';
    }

}

class get_coefficient_tests {

    public static function should_return_true_when_one_element_only() {
        $result = get_coefficient('<tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">1.00');

        if (count($result) == 0 || $result[0] != '1.00') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_more_than_one_element() {
        $result = get_coefficient('<div><tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">1.00</td></tr>
                                        <tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">01.00</td></tr>
                                        <tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">10.00</td></tr></div>');

        $expected = ['1.00', '01.00', '10.00'];

        if (count($result) != 3 || $result !== $expected) {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_key_data_is_padded_with_spaces() {
        $result = get_coefficient('<tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">   1.00   ');

        if (count($result) == 0 || $result[0] != '1.00') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_false_when_key_data_contains_alphabetic_chars() {
        $result = get_coefficient('<tr><td class="columnWithName">Test</td><td><div id="" class=""></div></td><td class="" style="">  test 1.00   ');

        if (count($result) != 0) {
            return 'failed';
        }
        return 'passed';
    }
}

class get_overall_tests {

    public static function should_return_true_when_one_element_only() {
        $result = get_overall('<tr><td class="columnWithName"></td><td><div id="" class=""></div></td><td class="" style=""><td class=""><span class="">1</span></td></tr>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_more_than_one_element() {
        $result = get_overall('<div><tr><td class="columnWithName"></td><td><div id="" class=""></div></td><td class="" style=""><td class=""><span class="">1</span></td></tr>
                                    <tr><td class="columnWithName"></td><td><div id="" class=""></div></td><td class="" style=""><td class=""><span class="">1</span></td></tr></div>');

        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i] != '1') {
                return 'failed';
            }
        }
        return 'passed';
    }

    public static function should_return_true_when_key_data_is_padded_with_spaces() {
        $result = get_overall('<tr><td class="columnWithName"></td><td><div id="" class=""></div></td><td class="" style=""><td class=""><span class="">     1     </span></td></tr>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

}

class get_reportees_tests {

    public static function should_return_true_when_one_element_only() {
        $result = get_reportees('<p><span class="reportees">1</span></p>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_more_than_one_element() {
        $result = get_reportees('<div><p><span class="reportees">1</span></p>
                                      <p><span class="reportees">1</span></p></div>');

        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i] != '1') {
                return 'failed';
            }
        }
        return 'passed';
    }

    public static function should_return_true_when_key_data_is_padded_with_spaces() {
        $result = get_reportees('<p><span class="reportees">     1     </span></p>');

        if (count($result) == 0 || $result[0] != '1') {
            return 'failed';
        }
        return 'passed';
    }

}

class connection_tests {

    public static function route_not_available_with_no_specific_request() {
        $url = 'localhost:8181/';
        $page = get_data($url);
        if (strpos($page,'error')!==false)
            return 'passed';
        return 'failed';
    }

    public static function route_not_available_with_wrong_service() {
        $url = 'localhost:8181/wrong-service/';
        $page = get_data($url);
        if (strpos($page,'error')!==false)
            return 'passed';
        return 'failed';
    }

    public static function route_not_available_with_wrong_option() {
        $url = 'localhost:8181/citizen_pov/no-option/';
        $page = get_data($url);
        if (strpos($page,'error')!==false)
            return 'passed';
        return 'failed';
    }

    public static function route_not_available_with_wrong_data() {
        $url = 'localhost:8181/citizen_pov/general_info/f2f4fet8a88fsd8ge9w8er';
        $page = get_data($url);
        if (strpos($page,'No data found')!==false)
            return 'passed';
        return 'failed';
    }

}

print_('get_categories_test_results');
print_('should_return_true_when_one_element_only: ' . get_categories_tests::should_return_true_when_one_element_only());
print_('should_return_true_when_more_than_one_element: ' . get_categories_tests::should_return_true_when_more_than_one_element());
print_('should_return_true_when_key_data_is_padded_with_spaces: ' . get_categories_tests::should_return_true_when_key_data_is_padded_with_spaces());
print_('should_return_false_when_key_data_is_not_enclosed_in_a_tr: ' . get_categories_tests::should_return_false_when_key_data_is_not_enclosed_in_a_tr());

print_('get_coefficient_test_results');
print_('should_return_true_when_one_element_only: ' . get_coefficient_tests::should_return_true_when_one_element_only());
print_('should_return_true_when_more_than_one_element: ' . get_coefficient_tests::should_return_true_when_more_than_one_element());
print_('should_return_true_when_key_data_is_padded_with_spaces: ' . get_coefficient_tests::should_return_true_when_key_data_is_padded_with_spaces());
print_('should_return_false_when_key_data_contains_alphabetic_chars: ' . get_coefficient_tests::should_return_false_when_key_data_contains_alphabetic_chars());

print_('get_overall_test_results');
print_('should_return_true_when_one_element_only: ' . get_overall_tests::should_return_true_when_one_element_only());
print_('should_return_true_when_more_than_one_element: ' . get_overall_tests::should_return_true_when_more_than_one_element());
print_('should_return_true_when_key_data_is_padded_with_spaces: ' . get_overall_tests::should_return_true_when_key_data_is_padded_with_spaces());

print_('get_reportees_test_results');
print_('should_return_true_when_one_element_only: ' . get_reportees_tests::should_return_true_when_one_element_only());
print_('should_return_true_when_more_than_one_element: ' . get_reportees_tests::should_return_true_when_more_than_one_element());
print_('should_return_true_when_key_data_is_padded_with_spaces: ' . get_reportees_tests::should_return_true_when_key_data_is_padded_with_spaces());

print_('connection_test_results');
print_('route_not_available_with_wrong_data: ' . connection_tests::route_not_available_with_wrong_data());
print_('route_not_available_with_wrong_option: ' . connection_tests::route_not_available_with_wrong_option());
print_('route_not_available_with_wrong_service: ' . connection_tests::route_not_available_with_wrong_service());
print_('route_not_available_with_no_specific_request: ' . connection_tests::route_not_available_with_no_specific_request());

?>