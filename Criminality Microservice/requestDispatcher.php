<?php

define('__ROOT__', dirname(__FILE__));
require_once('utilities/errorHandler.php');
require_once('utilities/dataRetriever.php');
require_once('utilities/parseRequest.php');
require_once('utilities/cache.php');

$requestURL = explode('/', substr($_SERVER['REQUEST_URI'], 1));
$requestService = strtolower($requestURL[0]);

if(empty($requestURL[0]))
{
   returnError(['error'=>'Must request a service, check documentation','doc'=>'urlToDoc']);
}

if (!isset($requestURL[1]))
{
    returnError('No option requested.');
}

$requestService = parse_($requestURL[0]);
$requestServiceFunction = parse_($requestURL[1]);
$requestService = __ROOT__."\\services\\".$requestService.".php";


if (file_exists($requestService)) {
    require_once($requestService);
    if (in_array($requestServiceFunction, get_defined_functions()["user"])) {
        $requestData = array_map(function ($x) { return strtolower($x); },(array_slice($requestURL, 2, null)));

        echo checkCache(parse_($requestURL[0]), $requestServiceFunction, $requestData);
    } else {
        returnError('No such function available.');
    }
}
else
{
    returnError('No such service available.');
}
