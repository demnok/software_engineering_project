<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 10-May-17
 * Time: 00:19
 */


function print_($data)
{
    print_r($data);
    echo PHP_EOL;
}


function short_info($page)
{

    $regex = '#[\d]{2,4}\s[\d]{2,3}\s[\d]{2,3}#';
    preg_match_all($regex, $page, $result);
    empty($result[0]) ? $phone = 'NA' : ($result[0][0] == '021 31 99') ? $phone = ['021 31 99 528 interior 134563'] : $phone = $result[0];
    preg_match('#Orar:.*[^<br>\n]#', $page, $result);
    return $phone;

}

class ShortInfoTests
{

    public static function should_return_true_when_one_phone_number()
    {
        $result = short_info("<h2>Informatii CFR Baia Mare - Telefon gara Baia Mare</h2>Telefon: <u>0746 246 471</u><br><br>Orar: non stop<br><br>");

        if (count($result) == 0 || $result[0] != '0746 246 471') {
            var_dump($result);
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_true_when_more_than_one_number()
    {
        $result = short_info("<h2>Informatii CFR Baia Mare - Telefon gara Baia Mare</h2>Telefon: <u>0746 246 471  --- 0746 246 471</u> 0746 246 471 <br><br>Orar: non stop<br><br>");

        for ($i = 0; $i < count($result); $i++) {
            if ($result[$i] != '0746 246 471') {
                return 'failed';
            }
        }
        return 'passed';
    }

    public static function should_return_true_when_number_is_padded_with_spaces()
    {
        $result = short_info('<h2>Informatii CFR Baia Mare - Telefon gara Baia Mare</h2>Telefon: <u>     0746 246 471       </td></tr>');

        if (count($result) == 0 || $result[0] != '0746 246 471') {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_false_when_number_is_not_enclosed()
    {
        $result = short_info('0746 246 471');

        if (count($result) != 0) {
            return 'failed';
        }
        return 'passed';
    }

    public static function should_return_when_bucharest()
    {
        $result = short_info('<h2>Informatii CFR Bucuresti - Telefon gara Bucuresti</h2>Telefon: <u>021 9521</u><br>Telefon 2: <u>021 31 99 528</u> interior 134563<br>        <br>');

        if (count($result) == 0) {
            return 'failed';
        }
        return 'passed';
    }



}


print_('short_info_test_results');
print_('should_return_true_when_one_phone_number: ' . ShortInfoTests::should_return_true_when_one_phone_number());
print_('should_return_true_when_more_than_one_number: ' . ShortInfoTests::should_return_true_when_more_than_one_number());
print_('should_return_true_when_number_is_padded_with_spaces: ' . ShortInfoTests::should_return_true_when_number_is_padded_with_spaces());
print_('should_return_when_bucharest: ' . ShortInfoTests::should_return_when_bucharest());