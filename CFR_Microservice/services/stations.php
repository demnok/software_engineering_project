<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03-May-17
 * Time: 08:29
 */

define("AVAILABLE_STATIONS", 'Alba-Iulia Craiova Sfantu-Gheorghe Alexandria Deva Sibiu Arad Drobeta-Turnu-Severin Slatina Bacau Focsani Slobozia Baia-Mare Galati Suceava Bistrita Giurgiu Targoviste Botosani Iasi Targu-Jiu Braila Oradea Targu-Mures Brasov Piatra-Neamt Timisoara Bucuresti Pitesti Tulcea Buzau Ploiesti Vaslui Calarasi Ramnicu-Valcea Zalau Cluj-Napoca Resita Constanta Satu-Mare');
$stations_command_list = ['short_info', 'gps'];


function short_info($stationName)
{
    /*
    header('Content-Type: text/html; charset=utf-8');
    var_dump($stationName);
    */

    if (strpos(strtolower(AVAILABLE_STATIONS), $stationName) === false) {
        return 'station not available';
    }
    $url = 'http://www.portal-info.ro/ghid/informatii-cfr/informatii-cfr-' . strtolower($stationName) . '.html';
    $page = getWebsite($url);
    $regex = '#[\d]{2,4}\s[\d]{2,3}\s[\d]{2,3}#';
    preg_match_all($regex, $page, $result);
    empty($result[0]) ? $phone = 'NA' : ($result[0][0] == '021 31 99') ? $phone = ['021 31 99 528 interior 134563'] : $phone = $result[0];
    preg_match('#Orar:.*[^<br>\n]#', $page, $result);
    $schedule = substr($result[0], 6);
    return ['phone' => $phone, 'schedule' => $schedule];

}

function gps($stationName)
{

    if (strpos(strtolower(AVAILABLE_STATIONS), $stationName) === false) {
        return 'station not available';
    }
    $url = 'http://www.portal-info.ro/ghid/informatii-cfr/informatii-cfr-' . strtolower($stationName) . '.html';
    $page = getWebsite($url);
    $regex = '#LatLng\([\d.,\s]*\)#';
    preg_match($regex, $page, $result);
    preg_match_all('#[\d]+[.][\d]+#', $result[0], $return_data);
    return ['latitude' => $return_data[0][0], 'longitude' => $return_data[0][1]];

}