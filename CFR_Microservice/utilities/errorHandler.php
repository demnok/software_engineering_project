<?php


function returnError($error)
{
    if (is_array($error))
    {
        header('Content-Type: application/json');
        echo json_encode($error);
        exit();
    }

    $returnData = ['error' => $error];
    header('Content-Type: application/json');
    echo json_encode($returnData);
    exit();
}