<?php

define('__ROOT__', dirname(__FILE__));
require_once('utilities/errorHandler.php');
require_once('utilities/dataRetriever.php');
require_once('utilities/parseRequest.php');
require_once('utilities/cacheManager.php');


$requestURL = explode('/', substr($_SERVER['REQUEST_URI'], 1));
$requestServiceName = strtolower($requestURL[0]);

if(empty($requestURL[0]))
{
   returnError(['error'=>'Must request a service, check documentation','doc'=>'bitbucket.org\demnok\software_engineering_project\wiki\CFR%20API%20Documentation']);
}

if (!isset($requestURL[1]))
{
    returnError('No option requested.');
}

$requestServiceName = parse_($requestURL[0]);
$requestServiceFunction = parse_($requestURL[1]);
$requestServicePath = __ROOT__."\\services\\".$requestServiceName.".php";

if (file_exists($requestServicePath)) {
    require_once($requestServicePath);
    if (in_array($requestServiceFunction, get_defined_functions()["user"]))
        {
        $requestData = array_map(function ($x) { return strtolower($x); },(array_slice($requestURL, 2, null)));
        header('Content-Type: application/json');
        echo checkCache($requestServiceName, $requestServiceFunction, $requestData);
    } else {
        returnError('No such function available.');
    }
}
else
{
    returnError('No such service available.');
}

